package protopopov.lab45.service;

import protopopov.lab45.model.UtilityService;
import protopopov.lab45.dao.UtilityServiceDAO;

import java.util.List;

public class UtilityServiceService {
    private static UtilityServiceDAO utilityServiceDAO;

    public UtilityServiceService() {
        utilityServiceDAO = new UtilityServiceDAO();
    }

    public void persist(UtilityService entity) {
        utilityServiceDAO.openCurrentSessionwithTransaction();
        utilityServiceDAO.persist(entity);
        utilityServiceDAO.closeCurrentSessionwithTransaction();
    }

    public void update(UtilityService entity) {
        utilityServiceDAO.openCurrentSessionwithTransaction();
        utilityServiceDAO.update(entity);
        utilityServiceDAO.closeCurrentSessionwithTransaction();
    }

    public UtilityService findById(Integer id) {
        utilityServiceDAO.openCurrentSession();
        UtilityService utilityService = utilityServiceDAO.findById(id);
        utilityServiceDAO.closeCurrentSession();
        return utilityService;
    }

    public void delete(Integer id) {
        utilityServiceDAO.openCurrentSessionwithTransaction();
        UtilityService utilityService = utilityServiceDAO.findById(id);
        utilityServiceDAO.delete(utilityService);
        utilityServiceDAO.closeCurrentSessionwithTransaction();
    }

    public List<UtilityService> findAll() {
        utilityServiceDAO.openCurrentSession();
        List<UtilityService> utilityServiceList = utilityServiceDAO.findAll();
        utilityServiceDAO.closeCurrentSession();
        return utilityServiceList;
    }

    public void deleteAll() {
        utilityServiceDAO.openCurrentSessionwithTransaction();
        utilityServiceDAO.deleteAll();
        utilityServiceDAO.closeCurrentSessionwithTransaction();
    }

    public UtilityServiceDAO utilityServiceDAO() {
        return utilityServiceDAO;
    }
}