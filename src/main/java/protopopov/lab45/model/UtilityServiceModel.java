package protopopov.lab45.model;

public class UtilityServiceModel {
    private Integer id;
    private String paymentDate;
    private String payerName;
    private String payerAddress;
    private String accountNumber;
    private String serviceName;
    private boolean hasDiscount;
    private Double meterReadings;
    private Double paymentAmount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerAddress() {
        return payerAddress;
    }

    public void setPayerAddress(String payerAddress) {
        this.payerAddress = payerAddress;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public boolean isHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    public Double getMeterReadings() {
        return meterReadings;
    }

    public void setMeterReadings(Double meterReadings) {
        this.meterReadings = meterReadings;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public UtilityServiceModel(String paymentDate, String payerName, String payerAddress, String accountNumber,
                               String serviceName, boolean hasDiscount, Double meterReadings, Double paymentAmount) {
        this.paymentDate = paymentDate;
        this.payerName = payerName;
        this.payerAddress = payerAddress;
        this.accountNumber = accountNumber;
        this.serviceName = serviceName;
        this.hasDiscount = hasDiscount;
        this.meterReadings = meterReadings;
        this.paymentAmount = paymentAmount;
    }

    public UtilityServiceModel() {
    }

    public String toString() {
        return "UtilityServiceModel: " +
                "id=" + id +
                ", paymentDate='" + paymentDate + '\'' +
                ", payerName='" + payerName + '\'' +
                ", payerAddress='" + payerAddress + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", hasDiscount=" + hasDiscount +
                ", meterReadings=" + meterReadings +
                ", paymentAmount=" + paymentAmount;
    }
}