package protopopov.lab45.model;

import jakarta.persistence.*;

@Entity
@Table(name = "utility_services")
public class UtilityService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "payment_date", nullable = false)
    private String paymentDate;

    @Column(name = "payer_name", nullable = false)
    private String payerName;

    @Column(name = "payer_address", nullable = false)
    private String payerAddress;

    @Column(name = "account_number", nullable = false)
    private String accountNumber;

    @Column(name = "service_name", nullable = false)
    private String serviceName;

    @Column(name = "has_discount", nullable = false)
    private Boolean hasDiscount;

    @Column(name = "meter_readings", nullable = true)
    private String meterReadings;

    @Column(name = "payment_amount", nullable = false)
    private Double paymentAmount;

    public UtilityService(String paymentDate, String payerName, String payerAddress, String accountNumber,
                          String serviceName, Boolean hasDiscount, String meterReadings, Double paymentAmount) {
        this.paymentDate = paymentDate;
        this.payerName = payerName;
        this.payerAddress = payerAddress;
        this.accountNumber = accountNumber;
        this.serviceName = serviceName;
        this.hasDiscount = hasDiscount;
        this.meterReadings = meterReadings;
        this.paymentAmount = paymentAmount;
    }

    public UtilityService() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerAddress() {
        return payerAddress;
    }

    public void setPayerAddress(String payerAddress) {
        this.payerAddress = payerAddress;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Boolean getHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(Boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    public String getMeterReadings() {
        return meterReadings;
    }

    public void setMeterReadings(String meterReadings) {
        this.meterReadings = meterReadings;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    @Override
    public String toString() {
        return "UtilityService{" +
                "id=" + id +
                ", paymentDate='" + paymentDate + '\'' +
                ", payerName='" + payerName + '\'' +
                ", payerAddress='" + payerAddress + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", hasDiscount=" + hasDiscount +
                ", meterReadings='" + meterReadings + '\'' +
                ", paymentAmount=" + paymentAmount +
                '}';
    }
}