package protopopov.lab45.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import protopopov.lab45.model.UtilityService;

public class UtilityServiceDAO implements UtilityServiceDAOInterface<UtilityService, Integer> {
    private Session currentSession;
    private Transaction currentTransaction;

    public UtilityServiceDAO() {
    }

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionwithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionwithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    private static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure("META-INF/hibernate.cfg.xml");
        configuration.addAnnotatedClass(UtilityService.class);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties());
        return configuration.buildSessionFactory(builder.build());
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void persist(UtilityService entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(UtilityService entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public UtilityService findById(Integer id) {
        UtilityService utilityService = (UtilityService) getCurrentSession().get(UtilityService.class, id);
        return utilityService;
    }

    @Override
    public void delete(UtilityService entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<UtilityService> findAll() {
        List<UtilityService> utilityServices = (List<UtilityService>) getCurrentSession().createQuery("from UtilityService").list();
        return utilityServices;
    }

    @Override
    public void deleteAll() {
        List<UtilityService> entityList = findAll();
        for (UtilityService entity : entityList) {
            delete(entity);
        }
    }
}