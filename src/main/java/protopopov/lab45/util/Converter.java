package protopopov.lab45.util;

import protopopov.lab45.model.UtilityService;
import protopopov.lab45.model.UtilityServiceModel;

public class Converter {
    public static void utilityServiceModelToEntity(UtilityService service, UtilityServiceModel model ) {
        service.setPaymentDate(model.getPaymentDate());
        service.setPayerName(model.getPayerName());
        service.setPayerAddress(model.getPayerAddress());
        service.setAccountNumber(model.getAccountNumber());
        service.setServiceName(model.getServiceName());
        service.setHasDiscount(model.isHasDiscount());
        service.setMeterReadings(String.valueOf(model.getMeterReadings()));
        service.setPaymentAmount(model.getPaymentAmount());
    }
}