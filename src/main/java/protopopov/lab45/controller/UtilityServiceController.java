package protopopov.lab45.controller;

import protopopov.lab45.util.Converter;
import protopopov.lab45.model.UtilityService;
import protopopov.lab45.model.UtilityServiceModel;
import protopopov.lab45.service.UtilityServiceService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/UtilityServiceController")
public class UtilityServiceController extends HttpServlet{
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/insert.jsp";
    private static String LIST_USER = "/showAll.jsp";
    private UtilityServiceService utilityService;

    public UtilityServiceController() {
        super();
        utilityService = new UtilityServiceService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        List<UtilityService> serviceList = new ArrayList<>();

        if (action.equalsIgnoreCase("delete")) {
            int serviceId = Integer.parseInt(request.getParameter("serviceId"));
            utilityService.delete(serviceId);
            forward = LIST_USER;
            serviceList = utilityService.findAll();
            request.setAttribute("services", serviceList);
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int serviceId = Integer.parseInt(request.getParameter("serviceId"));
            UtilityService service = utilityService.findById(serviceId);
            request.setAttribute("UtilityService", service);
        } else if (action.equalsIgnoreCase("showAll")) {
            forward = LIST_USER;
            serviceList = utilityService.findAll();
            request.setAttribute("services", serviceList);
        } else {
            forward = INSERT_OR_EDIT;
            request.setAttribute("services", serviceList);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UtilityServiceModel serviceModel = new UtilityServiceModel();
        serviceModel.setPaymentDate(request.getParameter("paymentDate"));
        serviceModel.setPayerName(request.getParameter("payerName"));
        serviceModel.setPayerAddress(request.getParameter("payerAddress"));
        serviceModel.setAccountNumber(request.getParameter("accountNumber"));
        serviceModel.setServiceName(request.getParameter("serviceName"));
        serviceModel.setHasDiscount(Boolean.parseBoolean(request.getParameter("hasDiscount")));
        serviceModel.setMeterReadings(Double.parseDouble(request.getParameter("meterReadings")));
        serviceModel.setPaymentAmount(Double.parseDouble(request.getParameter("paymentAmount")));

        // Перевірка обов'язкових полів
        if (serviceModel.getPaymentDate() == null || serviceModel.getPaymentDate().isEmpty()
                || serviceModel.getPayerName() == null || serviceModel.getPayerName().isEmpty()
                || serviceModel.getPayerAddress() == null || serviceModel.getPayerAddress().isEmpty()
                || serviceModel.getAccountNumber() == null || serviceModel.getAccountNumber().isEmpty()
                || serviceModel.getServiceName() == null || serviceModel.getServiceName().isEmpty()
                || Double.isNaN(serviceModel.getMeterReadings())
                || Double.isNaN(serviceModel.getPaymentAmount())) {

            request.setAttribute("errorMessage", "Please fill in all required fields.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dateFormat.parse(serviceModel.getPaymentDate());
        } catch (ParseException e) {
            request.setAttribute("errorMessage", "Invalid date format. Please use yyyy-MM-dd.");
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        // Якщо валідація пройшла успішно, продовжуємо обробку
        String serviceId = request.getParameter("serviceId");
        if (serviceId == null || serviceId.isEmpty()) {
            UtilityService serviceEntity = new UtilityService();
            Converter.utilityServiceModelToEntity(serviceEntity, serviceModel);
            utilityService.persist(serviceEntity); // Збереження нового об'єкта в базі даних
        } else {
            serviceModel.setId(Integer.parseInt(serviceId));
            UtilityService serviceEntity = utilityService.findById(serviceModel.getId());
            if (serviceEntity == null) {
                request.setAttribute("errorMessage", "Service not found.");
                RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
                view.forward(request, response);
                return;
            }
            Converter.utilityServiceModelToEntity(serviceEntity, serviceModel);
            utilityService.update(serviceEntity); // Оновлення існуючого об'єкта в базі даних
        }
        List<UtilityService> serviceList = utilityService.findAll();
        request.setAttribute("services", serviceList);

        RequestDispatcher view = request.getRequestDispatcher(LIST_USER);
        view.forward(request, response);
    }
}