package protopopov.lab45;

import protopopov.lab45.model.UtilityService;
import protopopov.lab45.service.UtilityServiceService;

import java.util.List;

public class App {

    public static void main(String[] args) {
        UtilityServiceService utilityServiceService = new UtilityServiceService();

        List<UtilityService> utilityServiceList = utilityServiceService.findAll();
        System.out.println("Усі комунальні послуги:");
        for (var utilityService : utilityServiceList) {
            System.out.println(utilityService);
        }
    }
}