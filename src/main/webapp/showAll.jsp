<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List, protopopov.lab45.model.UtilityService" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Комунальні послуги</title>
    <link rel="stylesheet" href="static/css/styles.css">
</head>
<body>
<div class="background"></div>
<div class="content">
    <h1>Комунальні послуги</h1>
    <div class="face">
        <article class="leftPanel">
            <a href="index.jsp" class="btn">Головна</a>
            <a href="UtilityServiceController?action=insert" class="btn">Додати</a>
        </article>
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Дата оплати</th>
                <th>Платник</th>
                <th>Адреса платника</th>
                <th>Номер рахунку</th>
                <th>Назва послуги</th>
                <th>Знижка</th>
                <th>Показник лічильника</th>
                <th>Сума оплати</th>
                <th>Дії</th>
            </tr>
            <%
                List<UtilityService> services = (List<UtilityService>) request.getAttribute("services");
                for (UtilityService service : services) {
            %>
            <tr>
                <td><%= service.getId() %></td>
                <td><%= service.getPaymentDate() %></td>
                <td><%= service.getPayerName() %></td>
                <td><%= service.getPayerAddress() %></td>
                <td><%= service.getAccountNumber() %></td>
                <td><%= service.getServiceName() %></td>
                <td><%= service.getHasDiscount() %></td>
                <td><%= service.getMeterReadings() %></td>
                <td><%= service.getPaymentAmount() %></td>
                <td>
                    <a href="UtilityServiceController?action=edit&serviceId=<%= service.getId() %>">Редагувати</a>
                    <a href="UtilityServiceController?action=delete&serviceId=<%= service.getId() %>">Видалити</a>
                </td>
            </tr>
            <% } %>
        </table>
    </div>
</div>
</body>
</html>