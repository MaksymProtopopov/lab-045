<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Головна сторінка</title>
    <link rel="stylesheet" href="static/css/styles.css">
</head>
<body>
<div class="background"></div>
<div class="content">
    <h1>Ласкаво просимо</h1>
    <div class="main-content">
        <p>Ласкаво просимо до системи керування комунальними послугами. Ви можете переглянути та керувати своїми комунальними послугами, використовуючи навігаційні кнопки нижче.</p>
        <a href="UtilityServiceController?action=showAll" class="btn">Переглянути послуги</a>
    </div>
</div>
</body>
</html>