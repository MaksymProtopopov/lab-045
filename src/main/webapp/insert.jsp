<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Додати/редагувати комунальну послугу</title>
    <link rel="stylesheet" href="static/css/styles.css">
</head>
<body>
<div class="background"></div>
<div class="content">
    <h1>Послуга</h1>
        <% if (errorMessage != null) { %>
    <div class="error-message"><%= errorMessage %></div>
        <% } %>
    <form action="UtilityServiceController" method="post">
        <input type="hidden" name="serviceId" value="${UtilityService.id}">
        <label for="paymentDate">Дата оплати:</label>
        <input type="text" id="paymentDate" name="paymentDate" value="${UtilityService.paymentDate}">
        <label for="payerName">Прізвище та ініціали платника:</label>
        <input type="text" id="payerName" name="payerName" value="${UtilityService.payerName}">
        <label for="payerAddress">Адреса платника:</label>
        <input type="text" id="payerAddress" name="payerAddress" value="${UtilityService.payerAddress}">
        <label for="accountNumber">Номер особового рахунку:</label>
        <input type="text" id="accountNumber" name="accountNumber" value="${UtilityService.accountNumber}">
        <label for="serviceName">Найменування послуги:</label>
        <input type="text" id="serviceName" name="serviceName" value="${UtilityService.serviceName}">
        <label for="hasDiscount">Наявність пільги:</label>
        <input type="text" id="hasDiscount" name="hasDiscount" value="${UtilityService.hasDiscount}">
        <label for="meterReadings">Показання приладів обліку:</label>
        <input type="text" id="meterReadings" name="meterReadings" value="${UtilityService.meterReadings}">
        <label for="paymentAmount">Сума оплати:</label>
        <input type="text" id="paymentAmount" name="paymentAmount" value="${UtilityService.paymentAmount}">
        <button type="submit" class="btn">Зберегти</button>
        <a href="UtilityServiceController?action=showAll" class="btn">Назад</a>
    </form>
</div>
</body>
</html>